import * as THREE from "three";
import { RGBELoader } from "three/examples/jsm/loaders/RGBELoader";
// 导入控制器，轨道控制器
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
// 导入模型解析器
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

class Base3d {
    constructor(selector, onFinish) {
        this.container = document.querySelector(selector);
        this.camera;
        this.scene;
        this.renderer;
        this.model;
        this.panzi;
        this.animateAction;
        this.clock = new THREE.Clock();
        this.onFinish = onFinish;
        this.init();
        this.animate();
        this.progressFn;
    }
    // 监听模型加载进度
    onProgress(fn) {
        this.progressFn = fn;
    }
    init() {
        // 初始化场景
        this.initScene();
        // 初始化相机
        this.initCamera();
        // 初始化渲染器
        this.initRenderer();
        // 初始化控制器,要等相机和渲染器加载完后才可以
        // this.initControls();
        // 添加物体模型
        this.addMesh();

        // 监听场景大小缩放,调整渲染尺寸
        window.addEventListener("resize", this.onWindowResize.bind(this));

        // 监听滚轮事件
        window.addEventListener("mousewheel", this.onMouseWheel.bind(this));
    }
    // 初始化场景
    initScene() {
        this.scene = new THREE.Scene();
        this.setEnvMap("000");
    }
    // 初始化相机
    initCamera() {
        this.camera = new THREE.PerspectiveCamera(
            45, // 角度 45最佳
            window.innerWidth / window.innerHeight, // 照相机比例
            0.25, // 最近距离
            200 // 最远距离
        ); // 透视相机
        this.camera.position.set(-1.8, 0.6, 2.7); // 摄像机初始化位置
    }
    // 初始化渲染器
    initRenderer() {
        this.renderer = new THREE.WebGLRenderer({ antialias: true }); // 抗锯齿
        // 设置屏幕像素比
        this.renderer.setPixelRatio(window.devicePixelRatio);
        // 渲染尺寸大小
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        // 色调映射 线性
        this.renderer.toneMapping = THREE.ACESFilmicToneMapping;
        // 曝光程度
        this.renderer.toneMappingExposure = 3;
        // 容器放进渲染器
        this.container.appendChild(this.renderer.domElement);
    }
    // 设置环境背景
    setEnvMap(hdr) {
        new RGBELoader().setPath("./files/hdr/").load(hdr + ".hdr", (texture) => {
            // 纹理映射 球星
            texture.mapping = THREE.EquirectangularRefractionMapping;
            this.scene.background = texture;
            this.scene.environment = texture;
        });
    }
    // 开始渲染
    render() {
        // 获取帧与帧的时间差
        let delta = this.clock.getDelta();
        this.mixer && this.mixer.update(delta);
        this.renderer.render(this.scene, this.camera);
    }
    // 逐帧渲染
    animate() {
        this.renderer.setAnimationLoop(this.render.bind(this));
    }
    // 初始化控制器
    initControls() {
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    }
    // 加载模型
    setModel(modelName) {
        // 加载器
        return new Promise((resolve, reject) => {
            const loader = new GLTFLoader().setPath("files/gltf/");
            loader.load(
                modelName,
                (gltf) => {
                    // 移除旧的模型
                    this.model && this.model.removeFromParent();
                    // gltf.scene.children[0]; 第一个为模型; 第四个是盘子
                    this.model = gltf.scene.children[0];
                    // 加载盘子
                    if ("bag2.glb" == modelName && !this.panzi) {
                        this.panzi = gltf.scene.children[5];

                        // 修改摄像头为模型摄像头
                        this.camera = gltf.cameras[0];
                        // 调用动画
                        this.mixer = new THREE.AnimationMixer(gltf.scene.children[1]); // 相当于 设置好关键帧的播放器
                        // 播放动画
                        this.animateAction = this.mixer.clipAction(gltf.animations[0]);
                        // 设置动画播放时长
                        this.animateAction.setDuration(20).setLoop(THREE.LoopOnce);
                        // 播放完成停止
                        this.animateAction.clampWhenFinished = true;
                        // 开始播放 改成鼠标滚动播放
                        // this.animateAction.play();

                        // 减低原本场景亮度灯光
                        this.spotlight1 = gltf.scene.children[2].children[0];
                        this.spotlight1.intensity = 1;
                        this.spotlight2 = gltf.scene.children[3].children[0];
                        this.spotlight2.intensity = 1;
                        this.spotlight3 = gltf.scene.children[4].children[0];
                        this.spotlight3.intensity = 1;
                    }
                    this.scene.add(gltf.scene);
                    resolve(this.modelName + "模型添加成功");
                },
                (e) => {
                    // 模型加载进度
                    this.progressFn(e);
                }
            );
        });
    }
    // 添加物体
    async addMesh() {
        let res = await this.setModel("bag2.glb");
        this.onFinish(res);
    }
    // 屏幕大小尺寸改变
    onWindowResize() {
        this.camera.aspect = window.innerWidth / window.innerHeight;
        this.camera.updateProjectionMatrix(); // 改变摄像机
        this.renderer.setSize(window.innerWidth, window.innerHeight); // 改变渲染器尺寸
    }
    //　鼠标滚轮事件
    onMouseWheel(e) {
        let timeScale = e.deltaY > 0 ? 1 : -1; // 前滚或者后滚
        // 在设置播放速度 -1.5倍 0倍 1.5倍这样的
        this.animateAction.setEffectiveTimeScale(timeScale);
        this.animateAction.paused = false;
        this.animateAction.play();
        // 慢停下来 延迟执行 防抖节流 鼠标0.3S没滚就停下
        if (this.timeoutid) {
            clearTimeout(this.timeoutid);
        }
        this.timeoutid = setTimeout(() => {
            this.animateAction.halt(0.5);
        }, 300);
    }
}

export default Base3d;
