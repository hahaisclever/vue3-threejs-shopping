import axios from 'axios';

const request = axios.create({
    baseURL: "http://api.cpengx.cn/metashop/api",
});

// 响应拦截器
request.interceptors.response.use((response)=>{
    if(response.status == 200){
        return response.data;
    }else{
        return response;
    }
},function (error) {
    return Promise.reject(error);
})

// 获取首页数据
export const getHomePage = (params)=>{
    return request({
        method: "GET",
        url: "/homepage",
        params,
    })
}

// 获取产品列表数据
export const getProdects = ()=>{
    return request({
        method: "GET",
        url: "/products",
    })
}
