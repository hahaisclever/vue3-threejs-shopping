import { createStore } from 'vuex';

// 创建一个新的 store 实例
const store = createStore({
  state () {
    return {
      count: 0,
      isFullscreen: false,
      buycarts: []
    }
  },
  // 同步改变state
  mutations: {
    increment (state,payload) {
      state.count += payload;
    },
    setFullscreen(state,payload){
      state.isFullscreen = payload;
    },
    addBuycarts(state,payLoad){
      state.buycarts.push(payLoad);
    },
    addBuycartsNum(state,payLoad){
      state.buycarts[payLoad].num++;
    },
    minuBuycartsNum(state,payLoad){
      state.buycarts[payLoad].num--;
      if(state.buycarts[payLoad].num == 0){
        state.buycarts.splice(payLoad,1);
      }
    }
  },
  // 计算属性
  getters: {
    totalPrice(state){
      //pre,item 上一次的价格
      let total = state.buycarts.reduce((pre, item) => {
        return pre + item.price * item.num;
      }, 0);
      return total;
    }
  },
  // 异步改变state
  actions: {
      asyncAdd(state,payload){
        setTimeout(() => {
            store.commit('increment', payload)
        }, 1000);
      },
  }
})

export default store;
